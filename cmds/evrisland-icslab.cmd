# - Required modules
require mrfioc2,2.2.1rc3
require busy,1.7.2_c596e1c
require evrisland,develop

# - Standard modules
require iocstats,3.1.16
require autosave,5.10.0
require caputlog,b544f92
require recsync,1.3.0-9705e52
require ess,0.0.1

epicsEnvSet(PREFIX, "TR-LLRF")
epicsEnvSet(PCI_SLOT, "0c:00.0")

iocshLoad "$(mrfioc2_DIR)/evr.iocsh" "P=$(PREFIX), PCIID=$(PCI_SLOT), $(INITARGS=)"
iocshLoad "$(autosave_DIR)/autosave.iocsh"  "AS_TOP=$(AS_TOP=/var/log/autosave), IOCNAME=$(PEVR)"


# - Load EVR Island application
dbLoadRecords("evrisland.template", "P=$(PREFIX):, EVRPREFIX=$(PREFIX):")

# - Load standard ESS modules
iocshLoad("$(iocstats_DIR)/iocStats.iocsh", "IOCNAME=$(PREFIX)")

var(reccastTimeout, 5.0)
var(reccastMaxHoldoff, 5.0)
epicsEnvSet("RECSYNCPREFIX", "$(PREFIX)")
iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(RECSYNCPREFIX)")

epicsEnvSet("AS_REMOTE", "/opt/autosave")
epicsEnvSet("AS_FOLDER", "$(PREFIX)")
iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(AS_REMOTE),IOCNAME=$(AS_FOLDER)")

## Logging environment variables
epicsEnvSet("LOG_SERVER_NAME", "$(LOG_SERVER_NAME=localhost)")
epicsEnvSet("ERRORLOG_SERVER_PORT", "$(ERRORLOG_SERVER_PORT=9001)")
epicsEnvSet("CAPUTLOG_SERVER_PORT", "$(CAPUTLOG_SERVER_PORT=9001)")
epicsEnvSet("FACNAME", "$(FACNAME=)")
iocshLoad("$(ess_DIR)/iocLog.iocsh", "LOG_INET=$(LOG_SERVER_NAME),LOG_INET_PORT=$(ERRORLOG_SERVER_PORT),FACNAME=$(FACILITY_NAME=),IOCNAME=$(PREFIX)")
iocshLoad("$(caPutLog_DIR)/caPutLog.iocsh", "LOG_INET=$(LOG_SERVER_NAME),LOG_INET_PORT=$(CAPUTLOG_SERVER_PORT)")

iocshLoad("$(evrisland_DIR)/evrisland-base.iocsh")

# - Start IOC
iocInit

# Load default configuration from timing
iocshLoad "$(mrfioc2_DIR)/evrr.iocsh"                   "P=$(PREFIX), INTREF=$(INTREF=)"
iocshLoad "$(mrfioc2_DIR)/evrtclkr.iocsh"               "P=$(PREFIX)"
