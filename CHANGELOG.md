# 2.5.0+0
* Include mechanism to support Piezo Start on Mixed Mode (ICSHWI-17605)

# 2.4.0+0
* Implement RF Blanking feature (ICSHWI-17119)

# 2.3.0+0
* Implement new backplane scheme to not use 5th and 6th lines (ICSHWI-17749)

# 2.2.1+0
* Fix Event counter behavior (ICSHWI-17608)

# 2.2.0+0
* Remove dependency from mrfioc2 (loaded on template)
* Implement new backplane scheme for Piezo (ICSHWI-17570)

# 2.1.1+0
* Fix issue with Modulator being stopped when in On Demand (and 14Hz mode selected)
* Block Start pulses when in Free Running

# 2.1.0+0
* Remove island mode (ICSHWI-13679)
* Include On-Demand for Mixed Mode (ICSHWI-10130)
* Load the latest value of Mixed Mode delay and use it for Moduator at 14Hz delay (ICSHWI-13062)

# 2.0.0+0
* Change evrisland snippet to be loaded with mrfioc2 from a generic
evr template (ICSHWI-13440)

# 1.10.1+0
* Fix how the initial waveform are set (labels and sequencer)

# 1.10.0+0
* Change the way to load initial values (now using only database)

# 1.9.0+0
* Include dependency on mrfioc2
* Fix startup snippet to allow internal reference (for loopback mode)

# 1.8.4
* Fix counter start for Mixed/Global mode

# 1.8.3
* Fix issue when starts the IOC in Mixed/Global mode (now for real)

# 1.8.2
* Reduces FIM trigger width to 250ns

# 1.8.1
* Fix issue when starts the IOC in Mixed/Global mode

# 1.8.0
* Include list of PVs to be archived

# 1.7.6
* Disable modulator at 14Hz when in island mode

# 1.7.5

* Include databuffer records

# 1.7.4

* Set default delay for modulator run at 14Hz

# 1.7.3

* Include option for MEBT

# 1.7.2

* Load the default values from a .sav file
* Use only 1 snippet for all IOCs (everisland-base)


# 1.7.1

* Allow to use modulator at 14 Hz
* Change mixed mode frequency to ao and add a readback

# 1.6.1

* New frequency events to accomodate new 1Hz event
